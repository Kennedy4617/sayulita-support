# Generated by Django 3.0.3 on 2020-11-30 06:42

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('repair', '0004_auto_20201129_2350'),
    ]

    operations = [
        migrations.AlterField(
            model_name='service',
            name='date',
            field=models.DateTimeField(default=datetime.datetime(2020, 11, 30, 0, 42, 2, 349072), verbose_name='fecha'),
        ),
    ]
