from django.shortcuts import render, redirect, reverse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout, authenticate
from django.contrib import messages
from .models import Service


@login_required
def homepage(request):
	return render(request, "repair/dashboard.html")


def register(request):
	if request.method == "POST":
		form = UserCreationForm(request.POST)
		if form.is_valid():
			user = form.save()
			login(request, user)
			return redirect("repair:homepage")
		else:
			for msg in form.error_messages:
				messages.error(request, f"{msg}: {form.error_messages[msg]}")

	form = UserCreationForm
	return render(request, "repair/register.html", {"form":form})


def login_request(request):
	if request.method == "POST":
		form = AuthenticationForm(request, data=request.POST)
		if form.is_valid():
			user = form.cleaned_data.get('username')
			pas = form.cleaned_data.get('password')
			key = authenticate(username=user, password=pas)

			if key is not None:
				login(request, key)
				return redirect("repair:homepage")
			else:
				messages.error(request, "Usuario y/o contraseña incorrecta.")
		else:
			messages.error(request, "Usuario y/o contraseña incorrecta.")

	form = AuthenticationForm()
	return render(request, "repair/login.html", {"form":form})


@login_required
def logout_request(request):
	logout(request)
	return redirect("repair:login")


def e404(request):
	return render(request, "repair/404.html")