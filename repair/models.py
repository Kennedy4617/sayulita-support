from django.db import models
from datetime import datetime


class Service(models.Model):
	"""
	Servicio
	"""
	DEVICE_TYPE_CHOICES = [
	    (1, 'Celular'),
	    (2, 'Tablet'),
	    (3, 'Laptop'),
	    (4, 'PC'),
	    (5, 'Consola'),
	    (6, 'Otro')
	]

	STATUES = [
	    (1, 'En Reparación'),
	    (2, 'Esperando Refacción'),
	    (3, 'Reparado'),
	    (4, 'Cancelado')
	]

	technical = models.CharField(max_length=100, verbose_name='técnico responsable')
	folio = models.CharField(max_length=20, verbose_name='número de folio')
	date = models.DateTimeField(verbose_name='fecha', default=datetime.now())
	client = models.CharField(max_length=100, verbose_name='nombre del cliente')
	serial = models.CharField(max_length=50, verbose_name='serial')
	brand = models.CharField(max_length=50, verbose_name='marca')
	model = models.CharField(max_length=50, verbose_name='modelo')
	device_type = models.IntegerField(choices=DEVICE_TYPE_CHOICES, verbose_name='tipo de dispositivo')
	password = models.CharField(max_length=20, verbose_name='contraseña')
	fix = models.CharField(max_length=250, verbose_name='falla del equipo')
	action = models.CharField(max_length=250, verbose_name='acción a tomar')
	warranty = models.IntegerField(verbose_name='días de garantía')
	cost = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='costo')
	advance = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='anticipo')
	total = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='total')
	statues = models.IntegerField(choices=STATUES, verbose_name='Estado')

	def __str__(self):
		return f'Folio: {self.folio} - Cliente: {self.client}'